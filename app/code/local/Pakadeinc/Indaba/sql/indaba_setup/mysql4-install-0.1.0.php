<?php

$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table indaba (indaba_id int not null auto_increment, title varchar(100), content text(200), link varchar(255), date datetime, status varchar(100),primary key(indaba_id));
    
SQLTEXT;

$installer->run($sql);

$installer->endSetup();


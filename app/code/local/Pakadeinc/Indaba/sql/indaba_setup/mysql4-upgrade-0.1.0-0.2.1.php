<?php

$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
alter table indaba add column image varchar(100) null after title, add column short_content text null after image;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();


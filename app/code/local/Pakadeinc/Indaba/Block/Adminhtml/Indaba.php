<?php
class Pakadeinc_Indaba_Block_Adminhtml_Indaba extends Mage_Adminhtml_Block_Widget_Grid_Container{

    public function __construct()
    {

        $this->_controller = "adminhtml_indaba";
        $this->_blockGroup = "indaba";
        $this->_headerText = Mage::helper("indaba")->__("Indaba News Manager");
        $this->_addButtonLabel = Mage::helper("indaba")->__("Add New Item");
        parent::__construct();

    }

}

<?php

class Pakadeinc_Indaba_Block_Adminhtml_Indaba_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {

        parent::__construct();
        $this->_objectId = "indaba_id";
        $this->_blockGroup = "indaba";
        $this->_controller = "adminhtml_indaba";
        $this->_updateButton("save", "label", Mage::helper("indaba")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("indaba")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label"     => Mage::helper("indaba")->__("Save And Continue Edit"),
            "onclick"   => "saveAndContinueEdit()",
            "class"     => "save",
        ), -100);

        $this->_formScripts[] = "function saveAndContinueEdit(){
                                    editForm.submit($('edit_form').action+'back/edit/');
                                 }";
    }

    public function getHeaderText()
    {
        if( Mage::registry("indaba_data") && Mage::registry("indaba_data")->getId() ){
            return Mage::helper("indaba")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("indaba_data")->getId()));
        }
        else{
            return Mage::helper("indaba")->__("Add Item");
        }
    }
}
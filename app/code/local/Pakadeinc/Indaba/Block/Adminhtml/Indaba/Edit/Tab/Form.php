<?php
class Pakadeinc_Indaba_Block_Adminhtml_Indaba_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("indaba_form",
            array("legend"=>Mage::helper("indaba")->__("Item information")));


        $fieldset->addField("title", "text", array(
            "label"     => Mage::helper("indaba")->__("Title"),
            "name"      => "title",
            'class'     => 'required-entry',
            'required'  => true,
        ));
        $fieldset->addField("link", "text", array(
            "label"     => Mage::helper("indaba")->__("Identifier"),
            "name"      => "link",
            'class'     => 'required-entry',
            'required'  => true,
        ));
        $fieldset->addField('image', 'file', array(
            'label'     => Mage::helper('indaba')->__('Feature Image'),
            'required'  => false,
            'name'      => 'image',
           // 'after_element_html' => 'Format 125 x 118 pixels',
        ));
        $fieldset->addField("short_content", "textarea", array(
            "label"     => Mage::helper("indaba")->__("Short Content"),
            "name"      => "short_content",
            'style'     => 'height:100px;',
            'style'     => 'width:694px;',
        ));

        $config = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array( 'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index') )
        );
        $fieldset->addField("description", "editor", array(
            "label"     => Mage::helper("indaba")->__("Content"),
            "name"      => "description",
            'style'     => 'height:400px;',
            'style'     => 'width:694px;',
            'wysiwyg'   => true,
            'config'    =>$config,
        ));

        $fieldset->addField("author", "text", array(
            "label"     => Mage::helper("indaba")->__("Author"),
            "name"      => "author",
            'class'     => 'required-entry',
            'required'  => true,
        ));
        $outputFormat = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);

        $fieldset->addField("date", "date", array(
            "label"     => Mage::helper("indaba")->__("Publish Date"),
            "name"      => "date",
            'class'     => 'required-entry  validate-date',
            'required'  => true,
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => $outputFormat,
            'time'      => true,
        ));

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('indaba')->__('Status'),
            'values'    => Pakadeinc_Indaba_Block_Adminhtml_Indaba_Grid::getValueArray2(),
            'name'      => 'status',
        ));

        if (Mage::getSingleton("adminhtml/session")->getIndabaData())
        {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getIndabaData());
            Mage::getSingleton("adminhtml/session")->setIndabaData(null);
        }
        elseif(Mage::registry("indaba_data")) {
            $form->setValues(Mage::registry("indaba_data")->getData());
        }
        return parent::_prepareForm();
    }

}
<?php
class Pakadeinc_Indaba_Block_Adminhtml_Indaba_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("indaba_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("indaba")->__("Item Information"));
    }
    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("indaba")->__("Item Information"),
            "title" => Mage::helper("indaba")->__("Item Information"),
            "content" => $this->getLayout()->
            createBlock("indaba/adminhtml_indaba_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}
<?php

class Pakadeinc_Indaba_Block_Adminhtml_Indaba_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId("indabaGrid");
        $this->setDefaultSort("indaba_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("indaba/indaba")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn("indaba_id", array(
            "header" => Mage::helper("indaba")->__("ID"),
            "align" =>"right",
            "width" => "50px",
            "type" => "number",
            "index" => "indaba_id",
        ));

        $this->addColumn("title", array(
        "header" => Mage::helper("indaba")->__("Title"),
        "index" => "title",
        ));
        $this->addColumn("date", array(
            "header" => Mage::helper("indaba")->__("Date"),
            "index" => "date",
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('indaba')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options'=>Pakadeinc_Indaba_Block_Adminhtml_Indaba_Grid::getOptionArray2(),
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }



    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('indaba_id');
        $this->getMassactionBlock()->setFormFieldName('indaba_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_indaba', array(
            'label'=> Mage::helper('indaba')->__('Remove Indaba'),
            'url'  => $this->getUrl('*/adminhtml_indaba/massRemove'),
            'confirm' => Mage::helper('indaba')->__('Are you sure?')
        ));
        return $this;
    }

    static public function getOptionArray2()
    {
        $data_array=array();
        $data_array[0]='Inactive';
        $data_array[1]='Active';
        return($data_array);
    }
    static public function getValueArray2()
    {
        $data_array=array();
        foreach(Pakadeinc_Indaba_Block_Adminhtml_Indaba_Grid::getOptionArray2() as $k=>$v){
            $data_array[]=array('value'=>$k,'label'=>$v);
        }
        return($data_array);

    }


}
<?php

class Pakadeinc_Indaba_Adminhtml_IndabaController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->
        _setActiveMenu("indaba/indaba")->_addBreadcrumb(Mage::helper("adminhtml")->
        __("Indaba  Manager"),Mage::helper("adminhtml")->__("Indaba Manager"));
        return $this;
    }
    public function indexAction()
    {
        $this->_title($this->__("Indaba"));
        $this->_title($this->__("Manager Indaba"));

        $this->_initAction();
        $this->renderLayout();
    }
    public function editAction()
    {
        $this->_title($this->__("Indaba"));
        $this->_title($this->__("Indaba"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("indaba/indaba")->load($id);
        if ($model->getId()) {
            Mage::register("indaba_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("indaba/indaba");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Indaba Manager"), Mage::helper("adminhtml")->__("Indaba Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Indaba Description"), Mage::helper("adminhtml")->__("Indaba Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->
            createBlock("indaba/adminhtml_indaba_edit"))->_addLeft($this->getLayout()->createBlock("indaba/adminhtml_indaba_edit_tabs"));
            $this->renderLayout();
        }
        else {
            Mage::getSingleton("adminhtml/session")->
            addError(Mage::helper("indaba")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("Indaba"));
        $this->_title($this->__("Indaba"));
        $this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
        $model  = Mage::getModel("indaba/indaba")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("indaba_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("indaba/indaba");

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        $this->_addBreadcrumb(Mage::helper("adminhtml")->
        __("Indaba Manager"), Mage::helper("adminhtml")->__("Indaba Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->
        __("Indaba Description"), Mage::helper("adminhtml")->__("Indaba Description"));


        $this->_addContent($this->getLayout()->
        createBlock("indaba/adminhtml_indaba_edit"))->_addLeft($this->getLayout()->createBlock("indaba/adminhtml_indaba_edit_tabs"));

        $this->renderLayout();

    }
    public function saveAction()
    {

        $post_data = $this->getRequest()->getPost();
        if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            try {
                /* Starting upload */
                $uploader = new Varien_File_Uploader('image');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->setAllowRenameFiles(false);

                // Set the file upload mode
                // false -> get the file directly in the specified folder
                // true -> get the file in the product like folders
                // (file.jpg will go in something like /media/f/i/file.jpg)
                $uploader->setFilesDispersion(false);

                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS ;
                if($uploader->save($path, $_FILES['image']['name'] )){
                    //return $uploader->getUploadedFileName();
                }

            } catch (Exception $e) {
                die($e->getMessage());
            }

            //this way the name is saved in DB
            $data['image'] = $_FILES['image']['name'];
        }
        $imageKey = array('image' => $data['image']);
        $post_data_final = array_merge($post_data, $imageKey);

        if ($post_data_final) {
            $model = Mage::getModel("indaba/indaba");
            $model->setData( $post_data_final)->setId($this->getRequest()->getParam('id'));
            try {
                //alter the date from post data to a format that will be accepted by the database
                $format = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
                if (isset( $post_data_final['date'])) {
                    $initialDate = Mage::app()->getLocale()->date( $post_data_final['date'], $format);
                    $model->setDate(Mage::getModel('core/date')->gmtDate(null, $initialDate->getTimestamp()));
                } else {
                    $model->setDate(Mage::getModel('core/date')->gmtDate());
                }

                $model = Mage::getModel("indaba/indaba")
                    ->addData( $post_data_final)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->
                addSuccess(Mage::helper("adminhtml")->__("Indaba was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setIndabaData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setIndabaData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }



    public function deleteAction()
    {
        if( $this->getRequest()->getParam("id") > 0 ) {
            try {
                $model = Mage::getModel("indaba/indaba");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->
                addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->
                addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('indaba_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("indaba/indaba");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->
            addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        }
        catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'indaba.csv';
        $grid       = $this->getLayout()->createBlock('indaba/adminhtml_indaba_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'indaba.xml';
        $grid       = $this->getLayout()->createBlock('indaba/adminhtml_indaba_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}
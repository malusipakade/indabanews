<?php
class Pakadeinc_Indaba_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {

        $this->loadLayout();

        $pageTitle = Mage::getStoreConfig('pakadeinc/pakadeinc_group/title');
        $this->getLayout()->getBlock("head")->setTitle($pageTitle);
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("indaba", array(
            "label" => $this->__($pageTitle),
            "title" => $this->__($pageTitle)
        ));
        // create a post view
        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'indaba',
            array('tempate' => 'indaba/content.phtml')
        );

        $this->getLayout()->getBlock('root')->setTamplate('page/1column.phtml');
        $this->getLayout()->getBlock('content')->append($block);


        // $this->_addContent($this->getLayout()->createBlock('pakadeinc_indaba/adminhtml_indaba'));
        $this->_initLayoutMessages('core/session');
        $this->renderLayout();
        //  Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles()); exit;

    }
}
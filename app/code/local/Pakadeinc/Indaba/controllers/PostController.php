<?php
class Pakadeinc_Indaba_PostController extends Mage_Core_Controller_Front_Action
{
    public function viewAction(){
        $this->loadLayout();

        $requestParams = $this->getRequest()->getParams();
        $postIdentifier = $requestParams['link'];

        $pageTitle = Mage::getStoreConfig('pakadeinc/pakadeinc_group/title');
        $this->getLayout()->getBlock("head")->setTitle($pageTitle);

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link"  => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("indaba", array(
            "label" => $this->__($pageTitle),
            "title" => $this->__($pageTitle),
            "link"  => Mage::getBaseUrl(). 'indaba/'
        ));

        $collection = Mage::getModel('indaba/indaba')->getCollection();
        foreach ($collection as $post) {
            if ($post->getData('link') == $postIdentifier){
                $breadcrumbs->addCrumb("post", array(
                    "label" => $this->__($post->getData('title')), //todo: get page title from configuration
                    "title" => $this->__($post->getData('title'))
                ));
            }
        }

        $this->renderLayout();
    }
}